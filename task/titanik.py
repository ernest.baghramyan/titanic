import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    title_groups = ["Mr.", "Mrs.", "Miss."]

    result = []

    for title in title_groups:
        median_age = df[df["Name"].str.contains(title)]["Age"].median()
        median_age = round(median_age)
        missing_values = df[(df["Name"].str.contains(title)) & df["Age"].isnull()]["Name"].count()
        result.append((title, missing_values, median_age))

    return result
